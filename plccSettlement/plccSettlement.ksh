#!/bin/ksh
########################################################################################################
#          Name: plccsettlement.sh
#   Description:storelist generation 
#       Company: Hot Topic
#  Modification: History:
#  Date          Name                       Modification Description
#  ------------  ------------------------   --------------------------------
# 28-aug-2017   pandu                 Initial Version
# 12-aug-2021	Infosys				  Updated for new PLCC format and added Type 11 record
########################################################################################################
#Global Variables

export ORACLE_HOME=/opt/oracle/product/11.2.0
export PATH=$PATH:$ORACLE_HOME/bin

DTMP=`date +%Y%m%d%H%M%S`
pgmName='plccsettlement.sh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
USAGE='USAGE: plccsettlement.sh'
scriptName=`basename $0`
#FORMSHOME=$MMHOME
#FileLoc=/opt/scripts/plccSettlement/pgpencrypter/build/input
FileLoc=/opt/scripts/plccSettlement/pgpencrypter/build/temp
LOGFILE=/opt/scripts/plccSettlement/logs/${pgmName}_${DTMP}.log
sqllog=/opt/scripts/plccSettlement/logs/${pgmName}_sql_${DTMP}.log
sqlerror=/opt/scripts/plccSettlement/logs/${pgmName}_error_${DTMP}.log
CONNECT=/@XOTDPRD_XCTR_INTFC

#echo $RUP

find /opt/scripts/plccSettlement/logs/${pgmName}*.log -mtime +1 -exec rm -rf {} \;

echo "---------------------------------------------------" > $LOGFILE
echo "Processing started at:"`date`                        >> $LOGFILE
echo "---------------------------------------------------" >> $LOGFILE

echo "Extracting full plcc export records" >> ${LOGFILE}
#item_exp_x=$FileLoc/PLCC_FULL_${DTMP}  
item_exp_x=$FileLoc/PROD.UR.TD2MN.ESTRECV.SETTLE
file_name=PLCC_FULL_${DTMP}.mnt


$ORACLE_HOME/bin/sqlplus -s $CONNECT <<END >> $sqllog

SET PAGESIZE 0 TRIMSPOOL ON TRIMOUT ON WRAP OFF TERMOUT OFF FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF UNDERLINE OFF LINESIZE 32767 PAGESIZE 40000 LONG 90000 NEWPAGE NONE long 32767 longchunksize 32767
  spool $item_exp_x.dat 
SELECT    record_type
       || RNK
       || hottopic
       || date_as
       || column1
       || column2
       || column3
       || column4
       || column5
       || column6
       || column7
       || column8
       || column9
       || column10
       || column11
       || column12
       || column13
       || column14
       || column15
       || column16
       || column17
       || column18
       || column19
       || column20
       || column21
       || column22
       || column23
       || column24
       || column25
       || column26
       || column27
       || column28
       || column29
       || column30
       || column31
       || column32
       || column33
       || column34
       || column35
       || column36
       || column37
       || column38
       || column39
       || column40
       || column41
       || column42
       || column43
       || column44
       || column45
       || column46
       || column47 FROM 
  (SELECT record_type
       , LPAD ( (DENSE_RANK () OVER (ORDER BY trans_seq ASC)), '7', '0') RNK
       , hottopic
       , date_as
       , column1
       , column2
       , column3
       , column4
       , column5
       , column6
       , column7
       , column8
       , column9
       , column10
       , column11
       , column12
       , column13
       , column14
       , column15
       , column16
       , column17
       , column18
       , column19
       , column20
       , column21
       , column22
       , column23
       , column24
       , column25
       , column26
       , column27
       , column28
       , column29
       , column30
       , column31
       , column32
       , column33
       , column34
       , column35
       , column36
       , column37
       , column38
       , column39
       , column40
       , column41
       , column42
       , column43
       , column44
       , column45
       , column46
       , column47
  FROM (SELECT '01' line_ind,
               '01' record_type,
               '000000000000000000' trans_seq,
               '000000000000000000' || RPAD ('TORRID', 40, ' ') hottopic,
               (SELECT TO_CHAR (SYSDATE-1, 'yyyymmdd') FROM DUAL) date_as,
               '002.66' column1,
               NULL column2,
               NULL column3,
               NULL column4,
               NULL column5,
               NULL column6,
               NULL column7,
               NULL column8,
               NULL column9,
               NULL column10,
               NULL column11,
               NULL column12,
               NULL column13,
               NULL column14,
               NULL column15,
               NULL column16,
               NULL column17,
               NULL column18,
               NULL column19,
               NULL column20,
               NULL column21,
               NULL column22,
               NULL column23,
               NULL column24,
               NULL column25,
               NULL column26,
               NULL column27,
               NULL column28,
               NULL column29,
               NULL column30,
               NULL column31,
               NULL column32,
               NULL column33,
               NULL column34,
               NULL column35,
               NULL column36,
               NULL column37,
               NULL column38,
               NULL column39,
               NULL column40,
               NULL column41,
               NULL column42,
               NULL column43,
               NULL column44,
               NULL column45,
               NULL column46,
               NULL column47
          FROM DUAL
        UNION all
          SELECT '10' line_ind,
                 record_type,
                 trans_seq,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 account_number,
                 tran_cd,
                 tran_amt,
                 LPAD (ABS (SUM (tax)), '9', '0') tax,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 LPAD (NVL(item_count,1), 3, 0),
                 order_date,
                 order_time,
                 payment_type,
                 pos_type,
                 reward_cnt,
                 reward_amt,
                 filler_4,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL
            FROM (  SELECT record_type,
                           trans_seq,
                           account_number,
                           tran_cd,
                           tran_amt,
                           tax,
                           postage,
                           tran_date,
                           tran_time,
                           promo_cd,
                           auth_no,
                           auth_buyer_seq_no,
                           invoice_no,
                           filler_1,
                           ring_cd,
                           defer_date,
                           po_job_no,
                           region,
                           district,
                           filler_2,
                           store_no,
                           filler_3,
                           clerk_id,
                           gl_origin_cd,               -- Torrid origin is 136
                           gl_type_cd,
                           -- lpad(TO_CHAR(sum(orig_invoice)),23,0) orig_invoice,
                           REPLACE (
                              LPAD (
                                 TO_CHAR (MIN (REPLACE (orig_invoice, ' ', 0))),
                                 23,
                                 0),
                              '00000000000000000000000',
                              '                       ')
                              orig_invoice,
                           item_count,
                           order_date, -- This is not required for POS transactions
                           order_time, -- This is not required for POS transactions
                           payment_type,
                           pos_type,
                           reward_cnt,
                           reward_amt,
                           filler_4
                      FROM (  SELECT '10' AS record_type,
                      -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return 
                                     CONCAT (
                                     CONCAT (
                                     CONCAT (
                                     CONCAT(-- Same auth number issue
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0')),
                                        LPAD (tp.string_value, '18', '0')),
                                        LPAD (im.auth_nbr, '7', '0')),
                                        LPAD(im.rtrans_lineitm_seq,'3','0'))-- Same auth number issue
                                        AS trans_seq,
                    -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                     (CASE
                                         WHEN tp.string_value IS NULL
                                         THEN
                                            '000000000000000001'
                                         ELSE
                                            LPAD (tp.string_value, '18', '0')
                                      END)
                                        AS account_number,
                                     (CASE
                                         WHEN ttr.tndr_statcode = 'Tender'
                                         THEN
                                            '202'
                                         WHEN ttr.tndr_statcode = 'Refund'
                                         THEN
                                            '602'
                                         ELSE
                                            '201'
                                      END)
                                        AS tran_cd,
                                     LPAD (ABS (ttr.amt * 100), 9, 0) AS tran_amt,
                                     REPLACE (ABS (it.taxtotal), CHR (46), NULL)
                                        AS tax,
                                     '000000000' AS postage,
                                     --TO_CHAR (it.business_date, 'yyyymmdd')
                                        --AS tran_date,
                                     TO_CHAR (TRUNC(new_time(it.CREATE_DATE,'GMT','PST')), 'yyyymmdd')
                                        AS tran_date,
                                     TO_CHAR (TRUNC(new_time(it.CREATE_DATE,'GMT','PST')), 'HHMMSS')
                                        AS tran_time,
                                     '00001' AS promo_cd,
                                     CASE
                                        WHEN im.auth_nbr IS NULL THEN '00000'
--XCR-1097 : Existing PLCC Settlemnet issue (Alpha Auth number )
										WHEN REGEXP_LIKE(LPAD (im.auth_nbr, '5', '0'), '[[:alpha:]]') THEN  '00000'
                                        ELSE LPAD (im.auth_nbr, '5', '0')
                                     END
                                        AS auth_no,
                                     '00000' AS auth_buyer_seq_no,
                                     CONCAT (
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0'))
                                        AS invoice_no,
                                     '   ' AS filler_1,
                                     '       ' AS ring_cd,
                                     '99990101' AS defer_date,
                                     '                    ' AS po_job_no,
                                     '00000' AS region,
                                     '00000' AS district,
                                     '0000' AS filler_2,
                                     LPAD (it.rtl_loc_id, '5', '0') AS store_no,
                                     '        ' AS filler_3,
                                     LPAD (it.operator_party_id, '7', '0')
                                        AS clerk_id,
                                     '136' AS gl_origin_cd, -- Torrid origin is 136
                                     '001' AS gl_type_cd,
                                     (CASE
                                         WHEN ttr.tndr_statcode = 'Refund'
                                         THEN
                                            TO_CHAR (
                                               CONCAT (
                                                  CONCAT (
                                                     LPAD (NVL(ts.original_rtl_loc_id,9999),
                                                           '5',
                                                           '0'),
                                                     LPAD (NVL(ts.original_wkstn_id,9),
                                                           '3',
                                                           '0')),
                                                  LPAD (NVL(ts.original_trans_seq,9999),
                                                        '15',
                                                        '0')))
                                         ELSE
                                            '                       '
                                      END)
                                        AS orig_invoice,
                                     --                                  TO_CHAR(concat(
                                     --                                      concat(lpad(ts.original_rtl_loc_id,'5','0'),lpad(ts.original_wkstn_id,'3','0')),
                                     --                                      lpad(ts.original_trans_seq,'15','0'))) AS orig_invoice,
                                     TO_CHAR (
                                        (  SELECT COUNT (*)
                                             FROM dtv.trl_rtrans_lineitm trl,
                                                  dtv.trl_sale_lineitm sl
                                            WHERE   -- 5th change TRUNC (trl.business_date) = TRUNC (SYSDATE-1)
                          TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                                                  AND trl.rtl_loc_id NOT IN ('5093')
                                                  AND trl.rtrans_lineitm_typcode =
                                                         'ITEM'
                                                  AND trl.void_flag = '0'
                                                  AND trl.trans_seq = it.trans_seq
                                                  AND trl.wkstn_id = it.wkstn_id
                                                  AND trl.rtl_loc_id = it.rtl_loc_id
                                                  AND trl.trans_seq = sl.trans_seq
                                                  AND trl.wkstn_id = sl.wkstn_id
                                                  AND trl.rtl_loc_id = sl.rtl_loc_id
                                                  AND trl.rtrans_lineitm_seq =
                                                         sl.rtrans_lineitm_seq
                                                  AND trl.business_date =
                                                         sl.business_date
                                                  AND sl.sale_lineitm_typcode <>
                                                         'ORDER'
                                                  AND sl.item_id NOT IN ('124', '608')
                                                  AND NOT EXISTS
                                                         (SELECT 1
                                                            FROM dtv.trl_rtrans_lineitm_p tlp
                                                           WHERE     tlp.organization_id =
                                                                        trl.organization_id
                                                                 AND tlp.trans_seq =
                                                                        trl.trans_seq
                                                                 AND tlp.rtl_loc_id =
                                                                        trl.rtl_loc_id
                                                                 AND tlp.wkstn_id =
                                                                        trl.wkstn_id
                                                                 AND tlp.rtrans_lineitm_seq =
                                                                        trl.rtrans_lineitm_seq
                                                                 AND tlp.property_code =
                                                                        'WEB_ITEM'
--XCR-1097 : Existing PLCC Settlemnet issue (Line item count mixmatch due to businessdate )
																		AND   tlp.business_date= trl.business_date)
                                       -- 5th change GROUP BY TO_CHAR (trl.business_date, 'dd-mon-yyyy'),
                                                 GROUP BY TO_CHAR (TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy'),
                                                  trl.trans_seq,
                                                  trl.wkstn_id,
                                                  trl.rtl_loc_id))
                                        AS item_count,
                                     '00010101' AS order_date, -- This is not required for POS transactions
                                     '000000' AS order_time, -- This is not required for POS transactions
                                     --                      ( CASE WHEN ts.sale_lineitm_typcode IN ('SALE','ORDER') THEN '000'
                                     --                           WHEN ts.sale_lineitm_typcode = 'RETURN'  THEN '000'
                                     --                      END ) AS payment_type,
                                     '0' AS payment_type,
                                     ' ' AS pos_type,
                                     '000' AS reward_cnt,
                                     '000000000' AS reward_amt,
                                     '             ' AS filler_4
                                FROM dtv.ttr_credit_debit_tndr_lineitm im,
                                     dtv.trn_trans it,
                                     dtv.trl_rtrans_lineitm_p tp,
                                     dtv.trl_rtrans_lineitm tl,
                                     dtv.ttr_tndr_lineitm ttr,
                                     dtv.trl_sale_lineitm ts
                               WHERE     tl.organization_id = im.organization_id
                                     AND tl.organization_id = it.organization_id
                                     AND tl.organization_id = tp.organization_id
                                     AND tl.organization_id = ttr.organization_id
                                     AND tl.organization_id = ts.organization_id
                                     AND tl.rtl_loc_id = im.rtl_loc_id
                                     AND tl.rtl_loc_id = it.rtl_loc_id
                                     AND tl.rtl_loc_id = tp.rtl_loc_id
                                     AND tl.rtl_loc_id = ttr.rtl_loc_id
                                     AND tl.rtl_loc_id = ts.rtl_loc_id
                                     AND tl.wkstn_id = im.wkstn_id
                                     AND tl.wkstn_id = it.wkstn_id
                                     AND tl.wkstn_id = tp.wkstn_id
                                     AND tl.wkstn_id = ttr.wkstn_id
                                     AND tl.wkstn_id = ts.wkstn_id
                                     AND tl.trans_seq = im.trans_seq
                                     AND tl.trans_seq = it.trans_seq
                                     AND tl.trans_seq = tp.trans_seq
                                     AND tl.trans_seq = ttr.trans_seq
                                     AND tl.trans_seq = ts.trans_seq
                                     AND tl.business_date = im.business_date
                                     AND tl.business_date = it.business_date
                                     AND tl.business_date = tp.business_date
                                     AND tl.business_date = ttr.business_date
                                     AND tl.business_date = ts.business_date
                                     AND tl.rtrans_lineitm_seq =
                                            tp.rtrans_lineitm_seq
                                     AND tl.rtrans_lineitm_seq =
                                            ttr.rtrans_lineitm_seq
                                     AND tl.rtrans_lineitm_seq =
                                            im.rtrans_lineitm_seq
                                     AND tp.property_code =
                                            'CLEARTEXT_ACCOUNT_NUMBER'
                                     AND NOT EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm_p tlp
                                              WHERE     tlp.organization_id =
                                                           tl.organization_id
                                                    AND tlp.trans_seq =
                                                           tl.trans_seq
                                                    AND tlp.rtl_loc_id =
                                                           tl.rtl_loc_id
                                                    AND tlp.wkstn_id = tl.wkstn_id
                                                    AND tlp.rtrans_lineitm_seq =
                                                           tl.rtrans_lineitm_seq
                                                    AND tlp.property_code IN ('WEB_ITEM',
                                                                              'WEB_ORDER_PAYMENT')
--XCR-1097 : Existing PLCC Settlemnet issue (Line item count mixmatch if transseq are same )
																			  AND   tlp.business_date= tl.business_date)
                                     AND tl.void_flag != '1'
                                     AND tl.rtrans_lineitm_typcode IN ('TENDER',
                                                                       'ITEM')
                                     AND im.organization_id = '1'
                                     AND ttr.tndr_id = 'PRIVATE_LABEL'
                                     AND it.post_void_flag = 0
                                     -- 5th change AND TRUNC (ttr.business_date) = TRUNC (SYSDATE-1)
                     AND TRUNC (new_time(ttr.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                                     AND ttr.rtl_loc_id NOT IN ('5093')
                                     AND ts.sale_lineitm_typcode IN ('SALE',
                                                                     'RETURN')
                                    and it.TRANS_STATCODE='COMPLETE'
                            --and it.trans_seq = 826 and it.wkstn_id = 1
                            GROUP BY it.organization_id,
                                     it.rtl_loc_id,
                                     it.wkstn_id,
                                     --it.business_date,
                                     TRUNC(new_time(it.CREATE_DATE,'GMT','PST')),
                                     it.trans_seq,
                                     tp.string_value,
                                     ttr.tndr_statcode,
                                     ttr.amt,
                                     it.taxtotal,
                                     it.trans_seq,
                                     im.auth_nbr,
                                     it.operator_party_id,
                                     --  ts.sale_lineitm_typcode,
                                     ts.original_wkstn_id,
                                     ts.original_rtl_loc_id,
                                     ts.original_trans_seq,
                                     im.rtrans_lineitm_seq)-- Same auth number issue
                  GROUP BY record_type,
                           trans_seq,
                           account_number,
                           tran_cd,
                           tran_amt,
                           tax,
                           postage,
                           tran_date,
                           tran_time,
                           promo_cd,
                           auth_no,
                           auth_buyer_seq_no,
                           invoice_no,
                           filler_1,
                           ring_cd,
                           defer_date,
                           po_job_no,
                           region,
                           district,
                           filler_2,
                           store_no,
                           filler_3,
                           clerk_id,
                           gl_origin_cd,               -- Torrid origin is 136
                           gl_type_cd,
                           item_count,
                           order_date, -- This is not required for POS transactions
                           order_time, -- This is not required for POS transactions
                           payment_type,
                           pos_type,
                           reward_cnt,
                           reward_amt,
                           filler_4
                  ORDER BY tran_date, invoice_no)
        GROUP BY record_type,
                 --  rownum,
                 trans_seq,
                 account_number,
                 tran_cd,
                 tran_amt,
                 postage,
                 tran_date,
                 tran_time,
                promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 item_count,
                 order_date,
                 order_time,
                 payment_type,
                 pos_type,
                 reward_cnt,
                 reward_amt,
                 filler_4
        UNION all 
          SELECT '10' line_ind,
                 record_type,
                 --  || lpad(rownum,'7','0') --TRANS_CNT
                 trans_seq,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 account_number,
                 tran_cd,
                 tran_amt,
                 LPAD (SUM (tax), '9', '0') tax,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 LPAD (item_count, 3, 0),
                 order_date,
                 order_time,
                 payment_type,
                 pos_type,
                 reward_cnt,
                 reward_amt,
                 filler_4,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL
            FROM (WITH temp_t1
                       AS (  SELECT '10' AS record_type,
                -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                   CONCAT (
                                     CONCAT (
                                     CONCAT (
                                     CONCAT(-- Same auth number issue
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0')),
                                        LPAD (tp.string_value, '18', '0')),
                                        LPAD (0, '7', '0')),-- Same auth number issue
                                        LPAD(0,'3','0'))-- Same auth number issue
                                       AS trans_seq,
                -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                    (CASE
                                        WHEN tp.string_value IS NULL
                                        THEN
                                           '000000000000000001'
                                        ELSE
                                           LPAD (tp.string_value, '18', '0')
                                     -- ELSE lpad(XCTR_INTFC.HTTD_XSTORE_PLCC(tp.STRING_VALUE),'18','0')
                                     END)
                                       AS account_number,
                                    --                (
                                    --                    CASE
                                    --                        WHEN ts.sale_lineitm_typcode = 'SALE'  THEN '852'
                                    --                    END
                                    --                )
                                    '852' AS tran_cd,
                                    --lpad(REPLACE(ttr.AMT,'.',''),'9','0')     AS TRAN_AMT,
                                    ttr.amt AS tran_amt,
                                    REPLACE (it.taxtotal, CHR (46), NULL) AS tax,
                                    '000000000' AS postage,
                                    --TO_CHAR (it.business_date, 'yyyymmdd')
                                      -- AS tran_date,
                                    TO_CHAR (TRUNC(new_time(it.CREATE_DATE,'GMT','PST')), 'yyyymmdd')
                                        AS tran_date,
                                    TO_CHAR (TRUNC(new_time(it.CREATE_DATE,'GMT','PST')), 'HHMMSS')
                                       AS tran_time,
                                    --lpad(it.trans_seq,'5','0')                AS PROMO_CD,
                                    '00001' AS promo_cd,
                                    '00000' AS auth_no,
                                    '00000' AS auth_buyer_seq_no,
                                    CONCAT (
                                       CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                               LPAD (it.wkstn_id, '3', '0')),
                                       LPAD (it.trans_seq, '12', '0'))
                                       AS invoice_no,
                                    '   ' AS filler_1,
                                    '       ' AS ring_cd,
                                    '99990101' AS defer_date,
                                    '                    ' AS po_job_no,
                                    '00000' AS region,
                                    '00000' AS district,
                                    '0000' AS filler_2,
                                    LPAD (it.rtl_loc_id, '5', '0') AS store_no,
                                    '        ' AS filler_3,
                                    LPAD (it.operator_party_id, '7', '0')
                                       AS clerk_id,
                                    '136' AS gl_origin_cd, -- Torrid origin is 136
                                    '001' AS gl_type_cd,
                                    --              (
                                    --                CASE
                                    --                   WHEN sale_lineitm_typcode = 'RETURN'  THEN TO_CHAR(original_trans_seq)
                                    --                   ELSE '                       '
                                    --              END
                                    --          )
                                    '                       ' AS orig_invoice,
                                    '0' AS item_count,
                                    '00010101' AS order_date, -- This is not required for POS transactions
                                    '000000' AS order_time, -- This is not required for POS transactions
                                    (CASE
                                        WHEN ttr.tndr_id = 'USD_CURRENCY'
                                        THEN
                                           '1'
                                        WHEN ttr.tndr_id = 'DEBIT_CARD'
                                        THEN
                                           '4'
                                        ELSE
                                           '6'
                                     END)
                                       AS payment_type,
                                    ' ' AS pos_type,
                                    '000' AS reward_cnt,
                                    '000000000' AS reward_amt,
                                    '                         ' AS filler_4
                               FROM dtv.trn_trans it,
                                    dtv.trl_sale_lineitm ts,
                                    dtv.ttr_tndr_lineitm ttr,
                                    dtv.trl_rtrans_lineitm tl,
                                    dtv.trl_rtrans_lineitm_p tp
                              WHERE TRUNC(new_time(it.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)    
                    -- 5th change it.business_date = TRUNC (SYSDATE-1)
                                    AND it.rtl_loc_id NOT IN ('5093')
                                    AND it.post_void_flag = 0
                                    AND ts.organization_id = it.organization_id
                                    AND ts.trans_seq = it.trans_seq
                                    AND ts.rtl_loc_id = it.rtl_loc_id
                                    AND ts.wkstn_id = it.wkstn_id
                                    AND ts.business_date = it.business_date
                                    AND ts.sale_lineitm_typcode IN ('SALE',
                                                                    'RETURN')
                                    AND ttr.organization_id = it.organization_id
                                    AND ttr.trans_seq = it.trans_seq
                                    AND ttr.rtl_loc_id = it.rtl_loc_id
                                    AND ttr.wkstn_id = it.wkstn_id
                                    AND ttr.business_date = it.business_date
                                    --   AND ttr.tndr_id = 'PRIVATE_LABEL'
                                    AND tl.organization_id = it.organization_id
                                    AND tl.trans_seq = it.trans_seq
                                    AND tl.rtl_loc_id = it.rtl_loc_id
                                    AND tl.wkstn_id = it.wkstn_id
                                    AND tl.business_date = it.business_date
                                    AND tl.void_flag = 0
                                    --          AND tl.rtrans_lineitm_typcode IN ('TENDER','ITEM')
                                    AND tl.rtrans_lineitm_typcode IN ('TENDER')
                                    --           AND tl.rtrans_lineitm_seq = im.rtrans_lineitm_seq
                                    AND tl.rtrans_lineitm_seq =
                                           ttr.rtrans_lineitm_seq
                                    AND tp.organization_id = tl.organization_id
                                    AND tp.trans_seq = tl.trans_seq
                                    AND tp.rtl_loc_id = tl.rtl_loc_id
                                    AND tp.wkstn_id = tl.wkstn_id
                                    AND tp.business_date = tl.business_date
                                    --     AND tp.rtrans_lineitm_seq = tl.rtrans_lineitm_seq
                                    AND tp.property_code <> 'WEB_ORDER_PAYMENT'
                                    AND tp.property_code =
                                           'CLEARTEXT_ACCOUNT_NUMBER'
                                    and it.TRANS_STATCODE='COMPLETE'
                                    --and it.trans_seq = 826 and it.wkstn_id = 1
                                    -- Code for Voided of PLCC paymnet line item 
                                    AND EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm tl
                                              WHERE     tl.organization_id =
                                                           ts.organization_id
                                                    AND tl.trans_seq =
                                                           ts.trans_seq
                                                    AND tl.rtl_loc_id =
                                                           ts.rtl_loc_id
                                                    AND tl.wkstn_id =
                                                           ts.wkstn_id
                                                    AND tl.business_Date =
                                                           ts.business_Date
                                                    AND tl.rtrans_lineitm_seq =
                                                           ts.rtrans_lineitm_seq
                                                    AND tl.VOID_FLAG = '0'
                                                     AND ts.item_id IN ('9999977','TCCPMNT'))
                                   --End OF PLCC payment Void Line  
                           GROUP BY it.trans_seq,
                                    tp.string_value,
                                    --       ts.sale_lineitm_typcode,
                                    ttr.tndr_statcode,
                                    ttr.amt,
                                    it.taxtotal,
                                    --it.business_date,
                                    TRUNC(new_time(it.CREATE_DATE,'GMT','PST')),
                                    it.trans_seq,
                                    --                  im.auth_nbr,
                                    it.rtl_loc_id,
                                    it.wkstn_id,
                                    it.operator_party_id,
                                    ts.original_wkstn_id,
                                    ts.original_rtl_loc_id,
                                    ts.original_trans_seq,
                                    ts.original_trans_seq,
                                    ttr.tndr_id
                                    --- Same amount in split tender 
                                    ,tl.rtrans_lineitm_seq
                           ORDER BY tran_date, invoice_no)
                    SELECT t.record_type,
                           t.trans_seq,
                           t.account_number,
                           t.tran_cd,
                           LPAD (ABS (SUM (t.tran_amt)) * 100, '9', '0') tran_amt,
                           t.tax,
                           t.postage,
                           t.tran_date,
                           t.tran_time,
                           t.promo_cd,
                           t.auth_no,
                           t.auth_buyer_seq_no,
                           t.invoice_no,
                           t.filler_1,
                           t.ring_cd,
                           t.defer_date,
                           t.po_job_no,
                           t.region,
                           t.district,
                           t.filler_2,
                           t.store_no,
                           t.filler_3,
                           t.clerk_id,
                           t.gl_origin_cd,
                           t.gl_type_cd,
                           t.orig_invoice,
                           t.item_count,
                           t.order_date,
                           t.order_time,
                           t.payment_type,
                           t.pos_type,
                           t.reward_cnt,
                           t.reward_amt,
                           t.filler_4
                      FROM temp_t1 t
                  GROUP BY t.record_type,
                           t.trans_seq,
                           t.account_number,
                           t.tran_cd,
                           t.tax,
                           t.postage,
                           t.tran_date,
                           t.tran_time,
                           t.promo_cd,
                           t.auth_no,
                           t.auth_buyer_seq_no,
                           t.invoice_no,
                           t.filler_1,
                           t.ring_cd,
                           t.defer_date,
                           t.po_job_no,
                           t.region,
                           t.district,
                           t.filler_2,
                           t.store_no,
                           t.filler_3,
                           t.clerk_id,
                           t.gl_origin_cd,
                           t.gl_type_cd,
                           t.orig_invoice,
                           t.item_count,
                           t.order_date,
                           t.order_time,
                           t.payment_type,
                           t.pos_type,
                           t.reward_cnt,
                           t.reward_amt,
                           t.filler_4)
        GROUP BY record_type,
                 trans_seq,
                 -- TRANS_CNT,
                 account_number,
                 tran_cd,
                 tran_amt,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 item_count,
                 order_date,
                 order_time,
                 payment_type,
                 pos_type,
                 reward_cnt,
                 reward_amt,
                 filler_4
        -- Record type 10 End --
           UNION all
                  SELECT
            '11' line_ind,
            record_type,
            trans_seq,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            account_number,
            pos_data_cd,
            card_type,
            ship_to_state,
            stan_code,
            filler_1,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
        FROM
            (
                SELECT
                    record_type,
                    trans_seq,
                    account_number,
                    '            ' AS pos_data_cd,
                    ' ' AS card_type,
                    '  ' AS ship_to_state,
                    stan_code,
                    '                                                                                                      ' AS filler_1,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL
                FROM
                    (
                        SELECT
                            '11' AS record_type,
                            concat(concat(concat(concat(-- Same auth number issue
                            concat(lpad(it.rtl_loc_id,'5','0'),lpad(it.wkstn_id,'3','0') ),lpad(it.trans_seq,'12','0') ),lpad(tp.string_value,'18','0') ),lpad(im
        .auth_nbr,'7','0') ),lpad(im.rtrans_lineitm_seq,'3','0') )
                             AS trans_seq, 
                            (
                                CASE
                                    WHEN tp.string_value IS NULL THEN '000000000000000001'
                                    ELSE lpad(tp.string_value,'18','0')
                                END
                            ) AS account_number,
                            '            ' AS pos_data_cd,
                            ' ' AS card_type,
                            '  ' AS ship_to_state,
                            '000000' AS stan_code,
                            '                                                                                                      ' AS filler_1
                        FROM
                            dtv.ttr_credit_debit_tndr_lineitm im,
                            dtv.trn_trans it,
                            dtv.trl_rtrans_lineitm_p tp,
                            dtv.trl_rtrans_lineitm tl,
                            dtv.ttr_tndr_lineitm ttr,
                            dtv.trl_sale_lineitm ts
                        WHERE
                            tl.organization_id = im.organization_id
                            AND   tl.organization_id = it.organization_id
                            AND   tl.organization_id = tp.organization_id
                            AND   tl.organization_id = ttr.organization_id
                            AND   tl.organization_id = ts.organization_id
                            AND   tl.rtl_loc_id = im.rtl_loc_id
                            AND   tl.rtl_loc_id = it.rtl_loc_id
                            AND   tl.rtl_loc_id = tp.rtl_loc_id
                            AND   tl.rtl_loc_id = ttr.rtl_loc_id
                            AND   tl.rtl_loc_id = ts.rtl_loc_id
                            AND   tl.wkstn_id = im.wkstn_id
                            AND   tl.wkstn_id = it.wkstn_id
                            AND   tl.wkstn_id = tp.wkstn_id
                            AND   tl.wkstn_id = ttr.wkstn_id
                            AND   tl.wkstn_id = ts.wkstn_id
                            AND   tl.trans_seq = im.trans_seq
                            AND   tl.trans_seq = it.trans_seq
                            AND   tl.trans_seq = tp.trans_seq
                            AND   tl.trans_seq = ttr.trans_seq
                            AND   tl.trans_seq = ts.trans_seq
                            AND   tl.business_date = im.business_date
                            AND   tl.business_date = it.business_date
                            AND   tl.business_date = tp.business_date
                            AND   tl.business_date = ttr.business_date
                            AND   tl.business_date = ts.business_date
                            AND   tl.rtrans_lineitm_seq = tp.rtrans_lineitm_seq
                            AND   tl.rtrans_lineitm_seq = ttr.rtrans_lineitm_seq
                            AND   tl.rtrans_lineitm_seq = im.rtrans_lineitm_seq
                            AND   tp.property_code = 'CLEARTEXT_ACCOUNT_NUMBER'
                            AND   NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    dtv.trl_rtrans_lineitm_p tlp
                                WHERE
                                    tlp.organization_id = tl.organization_id
                                    AND   tlp.trans_seq = tl.trans_seq
                                    AND   tlp.rtl_loc_id = tl.rtl_loc_id
                                    AND   tlp.wkstn_id = tl.wkstn_id
                                    AND   tlp.rtrans_lineitm_seq = tl.rtrans_lineitm_seq
                                    AND   tlp.property_code IN (
                                        'WEB_ITEM',
                                        'WEB_ORDER_PAYMENT'
                                    )
                                    AND   tlp.business_date = tl.business_date
                            )
                            AND   tl.void_flag != '1'
                            AND   tl.rtrans_lineitm_typcode IN (
                                'TENDER',
                                'ITEM'
                            )
                            AND   im.organization_id = '1'
                            AND   ttr.tndr_id = 'PRIVATE_LABEL'
                            AND   it.post_void_flag = 0
                            AND   trunc(new_time(ttr.create_date,'GMT','PST') ) = trunc(SYSDATE - 1)
                            AND   ttr.rtl_loc_id NOT IN ('5093')
                            AND   ts.sale_lineitm_typcode IN (
                                'SALE',
                                'RETURN'
                            )
                            AND   it.trans_statcode = 'COMPLETE'
                        GROUP BY
                            it.organization_id,
                            it.rtl_loc_id,
                            it.wkstn_id,
                            trunc(new_time(it.create_date,'GMT','PST') ),
                            it.trans_seq,
                            tp.string_value,
                            ttr.tndr_statcode,
                            ttr.amt,
                            it.taxtotal,
                            it.trans_seq,
                            im.auth_nbr,
                            it.operator_party_id,
                            ts.original_wkstn_id,
                            ts.original_rtl_loc_id,
                            ts.original_trans_seq,
                            im.rtrans_lineitm_seq
                    )
                GROUP BY
                    record_type,
                    trans_seq,
                    account_number,
                    stan_code 
                ORDER BY
                    trans_seq
            )        
        -- Record type 11 End --
        UNION all
          SELECT '30' line_ind,
                 '30',
                 --  || lpad(rownum,'7','0')
                 trans_seq,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 account_num,
               --  LISTAGG (item || quantity)
                  --  WITHIN GROUP (ORDER BY account_num, item, quantity)
rtrim(xmlagg(XMLELEMENT(e,item || quantity).EXTRACT('//text()')).GetClobVal()) 
FROM (  SELECT 
        -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                         CONCAT (
                           CONCAT (
                            CONCAT (
                            CONCAT(-- Same auth number issue
                              CONCAT (LPAD (tender.rtl_loc_id, '5', '0'),
                                      LPAD (tender.wkstn_id, '3', '0')),
                              LPAD (tender.trans_seq, '12', '0')),
                               LPAD (tender.string_value, '18', '0')),
                               LPAD (tender.auth_nbr, '7', '0')),
                               LPAD(tender.rtrans_lineitm_seq,'3','0'))-- Same auth number issue
                              AS trans_seq,
                -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                           CASE
                              WHEN tender.string_value IS NULL
                              THEN
                                 '000000000000000001'
                              ELSE
                                 LPAD (tender.string_value, '18', '0')
                           END
                              AS account_num,
                              (RPAD (lineitem.item_id, '30', ' '))
                           || (CASE
                                  WHEN lineitem.merch_level_1 = 'NP'
                                  THEN
                                     LPAD (99, '5', '0')
                                  ELSE
                                     LPAD (NVL (lineitem.merch_level_1,99), '5', '0') -- Default dept to 99
                               END)
                           || CASE
                                 WHEN tender.tndr_statcode = 'Refund'
                                 THEN
                                       '-'
                                    || LPAD ( --                              SUM(abs(lineitem.extended_amt * 100) ),
                                             ABS (lineitem.extended_amt * 100),
                                             '9',
                                             '0')
                                 WHEN tender.tndr_statcode = 'Tender'
                                 THEN
                                       '+'
                                    || LPAD ( --                               SUM(abs(lineitem.extended_amt * 100) ),
                                             ABS (lineitem.extended_amt * 100),
                                             '9',
                                             '0')
                              END
                              item,
                              REPLACE (LPAD ( --                         SUM(lineitem.quantity),
                                             lineitem.quantity, '5', '0'),
                                       '.',
                                       0)
                           || 'N'
                              quantity
                      FROM dtv.trl_sale_lineitm lineitem,
                           (SELECT DISTINCT trl.organization_id organization_id,
                                            --TRUNC (new_time(trl.CREATE_DATE,'GMT','PST')) business_date,
                                            trl.business_date business_date,
                                            trl.rtl_loc_id rtl_loc_id,
                                            trl.trans_seq trans_seq,
                                            trl.wkstn_id wkstn_id,
                                            ttr.string_value string_value,
                                            tr.tndr_statcode tndr_statcode,
                                            im.auth_nbr,
                                            im.rtrans_lineitm_seq-- Same auth number issue
                              FROM dtv.trl_rtrans_lineitm trl,
                                   dtv.trl_rtrans_lineitm_p ttr,
                                   dtv.ttr_tndr_lineitm tr,
                                   dtv.trn_trans trn,
                                   dtv.ttr_credit_debit_tndr_lineitm im
                             WHERE     trl.organization_id = ttr.organization_id
                                   AND trl.business_date = ttr.business_date
                                   AND trl.rtl_loc_id = ttr.rtl_loc_id
                                   AND trl.trans_seq = ttr.trans_seq
                                   AND trl.wkstn_id = ttr.wkstn_id
                                   AND trl.rtrans_lineitm_seq = ttr.rtrans_lineitm_seq
                                   AND property_code = 'CLEARTEXT_ACCOUNT_NUMBER'
                                   AND trl.organization_id = tr.organization_id
                                   AND trl.business_date = tr.business_date
                                   AND trl.rtl_loc_id = tr.rtl_loc_id
                                   AND trl.trans_seq = tr.trans_seq
                                   AND trl.wkstn_id = tr.wkstn_id
                                   AND trl.rtrans_lineitm_seq = tr.rtrans_lineitm_seq
                                    AND im.organization_id = tr.organization_id
                                   AND im.business_date = tr.business_date
                                   AND im.rtl_loc_id = tr.rtl_loc_id
                                   AND im.trans_seq = tr.trans_seq
                                   AND im.wkstn_id = tr.wkstn_id
                                   AND im.rtrans_lineitm_seq = tr.rtrans_lineitm_seq
                                   AND tr.tndr_id = 'PRIVATE_LABEL'
                                   AND trl.organization_id = trn.organization_id
                                   AND trl.business_date = trn.business_date
                                   AND trl.rtl_loc_id = trn.rtl_loc_id
                                   AND trl.trans_seq = trn.trans_seq
                                   AND trl.wkstn_id = trn.wkstn_id
                                   AND trn.post_void_flag = 0
                                   AND TRUNC (new_time(trl.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                   -- 5th change TRUNC (trl.business_date) = TRUNC (SYSDATE-1)
                                   AND trl.rtl_loc_id NOT IN ('5093')
                                   AND trl.rtrans_lineitm_typcode = 'TENDER'
                                    and trn.TRANS_STATCODE='COMPLETE'
                                   AND NOT EXISTS
                                          (SELECT 1
                                             FROM dtv.trl_rtrans_lineitm_p tlp
                                            WHERE     tlp.organization_id =
                                                         trl.organization_id
                                                  AND tlp.trans_seq =
                                                         trl.trans_seq
                                                  AND tlp.business_date =
                                                         trl.business_date
                                                  AND tlp.rtl_loc_id =
                                                         trl.rtl_loc_id
                                                  AND tlp.wkstn_id = trl.wkstn_id
                                                  AND tlp.rtrans_lineitm_seq =
                                                         trl.rtrans_lineitm_seq
                                                  AND tlp.property_code =
                                                         'WEB_ORDER_PAYMENT')
                                   AND trl.void_flag = '0' --AND trl.trans_seq=826 and trl.wkstn_id = 1
                                                          ) tender,
                           dtv.trl_rtrans_lineitm rtrn
                     WHERE     tender.organization_id = lineitem.organization_id
                           AND tender.business_date = lineitem.business_date
                           AND tender.rtl_loc_id = lineitem.rtl_loc_id
                           AND tender.trans_seq = lineitem.trans_seq
                           AND tender.wkstn_id = lineitem.wkstn_id
                           AND lineitem.sale_lineitm_typcode IN ('SALE', 'RETURN')
                           --           AND lineitem.trans_seq=224
                           AND rtrn.VOID_FLAG = 0
                           AND tender.organization_id = lineitem.organization_id
                           AND rtrn.business_date = lineitem.business_date
                           AND rtrn.rtl_loc_id = lineitem.rtl_loc_id
                           AND rtrn.trans_seq = lineitem.trans_seq
                           AND rtrn.wkstn_id = lineitem.wkstn_id
                           AND rtrn.RTRANS_LINEITM_SEQ =
                                  lineitem.RTRANS_LINEITM_SEQ
                    GROUP BY lineitem.item_id,
                           lineitem.merch_level_1,
                           lineitem.quantity,
                           lineitem.extended_amt,
                           lineitem.rtrans_lineitm_seq,
                           tender.string_value,
                           tender.tndr_statcode,
                           tender.wkstn_id,
                           tender.rtl_loc_id,
                           tender.trans_seq,
                           tender.auth_nbr,
                           tender.rtrans_lineitm_seq)-- Same auth number issue
        GROUP BY trans_seq, account_num
        UNION all
        SELECT '30' line_ind,
                 '30',
                 --  || lpad(rownum,'7','0')
                 trans_seq,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 account_num,
                 NULL
                 from (select A.account_number account_num ,b.account_num BA,A.trans_seq trans_seq from (SELECT '10' line_ind,
                 record_type,
                 trans_seq,
                 account_number,
                 tran_cd,
                 tran_amt,
                 LPAD (ABS (SUM (tax)), '9', '0') tax,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 LPAD (NVL(item_count,1), 3, 0),
                 order_date,
                 order_time,
                 payment_type,
                 filler_4
                FROM (  SELECT record_type,
                           trans_seq,
                           account_number,
                           tran_cd,
                           tran_amt,
                          tax,
                           postage,
                           tran_date,
                           tran_time,
                           promo_cd,
                           auth_no,
                           auth_buyer_seq_no,
                           invoice_no,
                           filler_1,
                           ring_cd,
                           defer_date,
                           po_job_no,
                           region,
                           district,
                           filler_2,
                           store_no,
                           filler_3,
                           clerk_id,
                           gl_origin_cd,               -- Torrid origin is 136
                           gl_type_cd,
                           -- lpad(TO_CHAR(sum(orig_invoice)),23,0) orig_invoice,
                           REPLACE (
                              LPAD (
                                 TO_CHAR (MIN (REPLACE (orig_invoice, ' ', 0))),
                                 23,
                                 0),
                              '00000000000000000000000',
                              '                       ')
                              orig_invoice,
                           item_count,
                           order_date, -- This is not required for POS transactions
                           order_time, -- This is not required for POS transactions
                           payment_type,
                           filler_4
                      FROM (  SELECT '10' AS record_type,
                                     -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return 
                                     CONCAT (
                                     CONCAT (
                                     CONCAT (
                                     CONCAT(-- Same auth number issue
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0')),
                                        LPAD (tp.string_value, '18', '0')),
                                        LPAD (im.auth_nbr, '7', '0')),
                                        LPAD(im.rtrans_lineitm_seq,'3','0'))-- Same auth number issue
                                        AS trans_seq,
                                     -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                     (CASE
                                         WHEN tp.string_value IS NULL
                                         THEN
                                            '000000000000000001'
                                         ELSE
                                            LPAD (tp.string_value, '18', '0')
                                      END)
                                        AS account_number,
                                     (CASE
                                         WHEN ttr.tndr_statcode = 'Tender'
                                         THEN
                                            '202'
                                         WHEN ttr.tndr_statcode = 'Refund'
                                         THEN
                                            '602'
                                         ELSE
                                            '201'
                                      END)
                                        AS tran_cd,
                                     LPAD (ABS (ttr.amt * 100), 9, 0) AS tran_amt,
                                     REPLACE (ABS (it.taxtotal), CHR (46), NULL)
                                        AS tax,
                                     '000000000' AS postage,
                                     TO_CHAR (it.business_date, 'yyyymmdd')
                                        AS tran_date,
                                     TO_CHAR (it.business_date, 'HHMMSS')
                                        AS tran_time,
                                     '00001' AS promo_cd,
                                     CASE
                                        WHEN im.auth_nbr IS NULL THEN '00000'
--XCR-1097 : Existing PLCC Settlemnet issue (Alpha Auth number )
										 WHEN REGEXP_LIKE(LPAD (im.auth_nbr, '5', '0'), '[[:alpha:]]') THEN  '00000'
                                        ELSE LPAD (im.auth_nbr, '5', '0')
                                     END
                                        AS auth_no,
                                     '00000' AS auth_buyer_seq_no,
                                     CONCAT (
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0'))
                                        AS invoice_no,
                                     '   ' AS filler_1,
                                     '       ' AS ring_cd,
                                     '99990101' AS defer_date,
                                     '                    ' AS po_job_no,
                                     '00000' AS region,
                                     '00000' AS district,
                                     '0000' AS filler_2,
                                     LPAD (it.rtl_loc_id, '5', '0') AS store_no,
                                     '        ' AS filler_3,
                                     LPAD (it.operator_party_id, '7', '0')
                                        AS clerk_id,
                                     '136' AS gl_origin_cd, -- Torrid origin is 136
                                     '001' AS gl_type_cd,
                                     (CASE
                                         WHEN ttr.tndr_statcode = 'Refund'
                                         THEN
                                            TO_CHAR (
                                               CONCAT (
                                                  CONCAT (
                                                     LPAD (NVL(ts.original_rtl_loc_id,9999),
                                                           '5',
                                                           '0'),
                                                     LPAD (NVL(ts.original_wkstn_id,9),
                                                           '3',
                                                           '0')),
                                                  LPAD (NVL(ts.original_trans_seq,9999),
                                                        '15',
                                                        '0')))
                                         ELSE
                                            '                       '
                                      END)
                                        AS orig_invoice,
                                     --                                  TO_CHAR(concat(
                                     --                                      concat(lpad(ts.original_rtl_loc_id,'5','0'),lpad(ts.original_wkstn_id,'3','0')),
                                     --                                      lpad(ts.original_trans_seq,'15','0'))) AS orig_invoice,
                                     TO_CHAR (
                                        (  SELECT COUNT (*)
                                             FROM dtv.trl_rtrans_lineitm trl,
                                                  dtv.trl_sale_lineitm sl
                                            WHERE   -- 5th change TRUNC (trl.business_date) = TRUNC (SYSDATE-1)
                        TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                                                  AND trl.rtl_loc_id NOT IN ('5093')
                                                  AND trl.rtrans_lineitm_typcode =
                                                         'ITEM'
                                                  AND trl.void_flag = '0'
                                                  AND trl.trans_seq = it.trans_seq
                                                  AND trl.wkstn_id = it.wkstn_id
                                                  AND trl.rtl_loc_id = it.rtl_loc_id
                                                  AND trl.trans_seq = sl.trans_seq
                                                  AND trl.wkstn_id = sl.wkstn_id
                                                  AND trl.rtl_loc_id = sl.rtl_loc_id
                                                  AND trl.rtrans_lineitm_seq =
                                                         sl.rtrans_lineitm_seq
                                                  AND trl.business_date =
                                                         sl.business_date
                                                  AND sl.sale_lineitm_typcode <>
                                                         'ORDER'
                                                  AND sl.item_id NOT IN ('124', '608')
                                                  AND NOT EXISTS
                                                         (SELECT 1
                                                            FROM dtv.trl_rtrans_lineitm_p tlp
                                                           WHERE     tlp.organization_id =
                                                                        trl.organization_id
                                                                 AND tlp.trans_seq =
                                                                        trl.trans_seq
                                                                 AND tlp.rtl_loc_id =
                                                                        trl.rtl_loc_id
                                                                 AND tlp.wkstn_id =
                                                                        trl.wkstn_id
                                                                 AND tlp.rtrans_lineitm_seq =
                                                                        trl.rtrans_lineitm_seq
                                                                 AND tlp.property_code =
                                                                        'WEB_ITEM')
                                        -- 5th change GROUP BY TO_CHAR (trl.business_date, 'dd-mon-yyyy'),
                        GROUP BY TO_CHAR (TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy'),
                                                  trl.trans_seq,
                                                  trl.wkstn_id,
                                                  trl.rtl_loc_id))
                                        AS item_count,
                                     '00010101' AS order_date, -- This is not required for POS transactions
                                     '000000' AS order_time, -- This is not required for POS transactions
                                     --                      ( CASE WHEN ts.sale_lineitm_typcode IN ('SALE','ORDER') THEN '000'
                                     --                           WHEN ts.sale_lineitm_typcode = 'RETURN'  THEN '000'
                                     --                      END ) AS payment_type,
                                     '000' AS payment_type,
                                     '                         ' AS filler_4
                                FROM dtv.ttr_credit_debit_tndr_lineitm im,
                                     dtv.trn_trans it,
                                     dtv.trl_rtrans_lineitm_p tp,
                                     dtv.trl_rtrans_lineitm tl,
                                     dtv.ttr_tndr_lineitm ttr,
                                     dtv.trl_sale_lineitm ts
                               WHERE     tl.organization_id = im.organization_id
                                     AND tl.organization_id = it.organization_id
                                     AND tl.organization_id = tp.organization_id
                                     AND tl.organization_id = ttr.organization_id
                                     AND tl.organization_id = ts.organization_id
                                     AND tl.rtl_loc_id = im.rtl_loc_id
                                     AND tl.rtl_loc_id = it.rtl_loc_id
                                     AND tl.rtl_loc_id = tp.rtl_loc_id
                                     AND tl.rtl_loc_id = ttr.rtl_loc_id
                                     AND tl.rtl_loc_id = ts.rtl_loc_id
                                     AND tl.wkstn_id = im.wkstn_id
                                     AND tl.wkstn_id = it.wkstn_id
                                     AND tl.wkstn_id = tp.wkstn_id
                                     AND tl.wkstn_id = ttr.wkstn_id
                                     AND tl.wkstn_id = ts.wkstn_id
                                     AND tl.trans_seq = im.trans_seq
                                     AND tl.trans_seq = it.trans_seq
                                     AND tl.trans_seq = tp.trans_seq
                                     AND tl.trans_seq = ttr.trans_seq
                                     AND tl.trans_seq = ts.trans_seq
                                     AND tl.business_date = im.business_date
                                     AND tl.business_date = it.business_date
                                     AND tl.business_date = tp.business_date
                                     AND tl.business_date = ttr.business_date
                                     AND tl.business_date = ts.business_date
                                     AND tl.rtrans_lineitm_seq =
                                            tp.rtrans_lineitm_seq
                                     AND tl.rtrans_lineitm_seq =
                                            ttr.rtrans_lineitm_seq
                                     AND tl.rtrans_lineitm_seq =
                                            im.rtrans_lineitm_seq
                                     AND tp.property_code =
                                            'CLEARTEXT_ACCOUNT_NUMBER'
                                     AND NOT EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm_p tlp
                                              WHERE     tlp.organization_id =
                                                           tl.organization_id
                                                    AND tlp.trans_seq =
                                                           tl.trans_seq
                                                    AND tlp.rtl_loc_id =
                                                           tl.rtl_loc_id
                                                    AND tlp.wkstn_id = tl.wkstn_id
                                                    AND tlp.rtrans_lineitm_seq =
                                                           tl.rtrans_lineitm_seq
                                                    AND tlp.property_code IN ('WEB_ITEM',
                                                                              'WEB_ORDER_PAYMENT'))
                                     AND tl.void_flag != '1'
                                     AND tl.rtrans_lineitm_typcode IN ('TENDER',
                                                                       'ITEM')
                                     AND im.organization_id = '1'
                                     AND ttr.tndr_id = 'PRIVATE_LABEL'
                                     AND it.post_void_flag = 0
                                     -- 5th change AND TRUNC (ttr.business_date) = TRUNC (SYSDATE-1)
                                     AND TRUNC (new_time(ttr.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                                     AND ttr.rtl_loc_id NOT IN ('5093')
                                     AND ts.sale_lineitm_typcode IN ('SALE',
                                                                     'RETURN')
                                    and it.TRANS_STATCODE='COMPLETE'
                            --and it.trans_seq = 826 and it.wkstn_id = 1
                            GROUP BY it.organization_id,
                                     it.rtl_loc_id,
                                     it.wkstn_id,
                                     it.business_date,
                                     it.trans_seq,
                                     tp.string_value,
                                     ttr.tndr_statcode,
                                     ttr.amt,
                                     it.taxtotal,
                                     it.trans_seq,
                                     im.auth_nbr,
                                     it.operator_party_id,
                                     --  ts.sale_lineitm_typcode,
                                     ts.original_wkstn_id,
                                     ts.original_rtl_loc_id,
                                     ts.original_trans_seq,
                                     im.rtrans_lineitm_seq)-- Same auth number issue
                  GROUP BY record_type,
                           trans_seq,
                           account_number,
                           tran_cd,
                           tran_amt,
                           tax,
                           postage,
                           tran_date,
                           tran_time,
                           promo_cd,
                           auth_no,
                           auth_buyer_seq_no,
                           invoice_no,
                           filler_1,
                           ring_cd,
                           defer_date,
                           po_job_no,
                           region,
                           district,
                           filler_2,
                           store_no,
                           filler_3,
                           clerk_id,
                           gl_origin_cd,               -- Torrid origin is 136
                           gl_type_cd,
                           item_count,
                           order_date, -- This is not required for POS transactions
                           order_time, -- This is not required for POS transactions
                           payment_type,
                           filler_4
                  ORDER BY tran_date, invoice_no)
        GROUP BY record_type,
                 --  rownum,
                 trans_seq,
                 account_number,
                 tran_cd,
                 tran_amt,
                 postage,
                 tran_date,
                 tran_time,
                promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 item_count,
                 order_date,
                 order_time,
                 payment_type,
                 filler_4
        UNION all 
          SELECT '10' line_ind,
                 record_type,
                 --  || lpad(rownum,'7','0') --TRANS_CNT
                 trans_seq,
                 account_number,
                 tran_cd,
                 tran_amt,
                 LPAD (SUM (tax), '9', '0') tax,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 LPAD (item_count, 3, 0),
                 order_date,
                 order_time,
                 payment_type,
                 filler_4
            FROM (WITH temp_t1
                       AS (  SELECT '10' AS record_type,
                                    -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                   CONCAT (
                                     CONCAT (
                                     CONCAT (
                                     CONCAT(-- Same auth number issue
                                        CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                                LPAD (it.wkstn_id, '3', '0')),
                                        LPAD (it.trans_seq, '12', '0')),
                                        LPAD (tp.string_value, '18', '0')),
                                        LPAD (0, '7', '0')),
                                        LPAD(0,'3','0'))-- Same auth number issue
                                       AS trans_seq,
                                     -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                    (CASE
                                        WHEN tp.string_value IS NULL
                                        THEN
                                           '000000000000000001'
                                        ELSE
                                           LPAD (tp.string_value, '18', '0')
                                     -- ELSE lpad(XCTR_INTFC.HTTD_XSTORE_PLCC(tp.STRING_VALUE),'18','0')
                                     END)
                                       AS account_number,
                                    --                (
                                    --                    CASE
                                    --                        WHEN ts.sale_lineitm_typcode = 'SALE'  THEN '852'
                                    --                    END
                                    --                )
                                    '852' AS tran_cd,
                                    --lpad(REPLACE(ttr.AMT,'.',''),'9','0')     AS TRAN_AMT,
                                    ttr.amt AS tran_amt,
                                    REPLACE (it.taxtotal, CHR (46), NULL) AS tax,
                                    '000000000' AS postage,
                                    TO_CHAR (it.business_date, 'yyyymmdd')
                                       AS tran_date,
                                    TO_CHAR (it.business_date, 'HHMMSS')
                                       AS tran_time,
                                    --lpad(it.trans_seq,'5','0')                AS PROMO_CD,
                                    '00001' AS promo_cd,
                                    '00000' AS auth_no,
                                    '00000' AS auth_buyer_seq_no,
                                    CONCAT (
                                       CONCAT (LPAD (it.rtl_loc_id, '5', '0'),
                                               LPAD (it.wkstn_id, '3', '0')),
                                       LPAD (it.trans_seq, '12', '0'))
                                       AS invoice_no,
                                    '   ' AS filler_1,
                                    '       ' AS ring_cd,
                                    '99990101' AS defer_date,
                                    '                    ' AS po_job_no,
                                    '00000' AS region,
                                    '00000' AS district,
                                    '0000' AS filler_2,
                                    LPAD (it.rtl_loc_id, '5', '0') AS store_no,
                                    '        ' AS filler_3,
                                    LPAD (it.operator_party_id, '7', '0')
                                       AS clerk_id,
                                    '136' AS gl_origin_cd, -- Torrid origin is 136
                                    '001' AS gl_type_cd,
                                    --              (
                                    --                CASE
                                    --                   WHEN sale_lineitm_typcode = 'RETURN'  THEN TO_CHAR(original_trans_seq)
                                    --                   ELSE '                       '
                                    --              END
                                    --          )
                                    '                       ' AS orig_invoice,
                                    '000' AS item_count,
                                    '00010101' AS order_date, -- This is not required for POS transactions
                                    '000000' AS order_time, -- This is not required for POS transactions
                                    (CASE
                                        WHEN ttr.tndr_id = 'USD_CURRENCY'
                                        THEN
                                           '001'
                                        WHEN ttr.tndr_id = 'DEBIT_CARD'
                                        THEN
                                           '004'
                                        ELSE
                                           '006'
                                     END)
                                       AS payment_type,
                                    '                         ' AS filler_4
                               FROM dtv.trn_trans it,
                                    dtv.trl_sale_lineitm ts,
                                    dtv.ttr_tndr_lineitm ttr,
                                    dtv.trl_rtrans_lineitm tl,
                                    dtv.trl_rtrans_lineitm_p tp
                               WHERE       TRUNC(new_time(it.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)    
                            -- 5th change it.business_date = TRUNC (SYSDATE-1)
                                    AND it.rtl_loc_id NOT IN ('5093')
                                    AND it.post_void_flag = 0
                                    AND ts.organization_id = it.organization_id
                                    AND ts.trans_seq = it.trans_seq
                                    AND ts.rtl_loc_id = it.rtl_loc_id
                                    AND ts.wkstn_id = it.wkstn_id
                                    AND ts.business_date = it.business_date
                                    AND ts.sale_lineitm_typcode IN ('SALE',
                                                                    'RETURN')
                                    AND ttr.organization_id = it.organization_id
                                    AND ttr.trans_seq = it.trans_seq
                                    AND ttr.rtl_loc_id = it.rtl_loc_id
                                    AND ttr.wkstn_id = it.wkstn_id
                                    AND ttr.business_date = it.business_date
                                    --   AND ttr.tndr_id = 'PRIVATE_LABEL'
                                    AND tl.organization_id = it.organization_id
                                    AND tl.trans_seq = it.trans_seq
                                    AND tl.rtl_loc_id = it.rtl_loc_id
                                    AND tl.wkstn_id = it.wkstn_id
                                    AND tl.business_date = it.business_date
                                    AND tl.void_flag = 0
                                    --          AND tl.rtrans_lineitm_typcode IN ('TENDER','ITEM')
                                    AND tl.rtrans_lineitm_typcode IN ('TENDER')
                                    --           AND tl.rtrans_lineitm_seq = im.rtrans_lineitm_seq
                                    AND tl.rtrans_lineitm_seq =
                                           ttr.rtrans_lineitm_seq
                                    AND tp.organization_id = tl.organization_id
                                    AND tp.trans_seq = tl.trans_seq
                                    AND tp.rtl_loc_id = tl.rtl_loc_id
                                    AND tp.wkstn_id = tl.wkstn_id
                                    AND tp.business_date = tl.business_date
                                    --     AND tp.rtrans_lineitm_seq = tl.rtrans_lineitm_seq
                                    AND tp.property_code <> 'WEB_ORDER_PAYMENT'
                                    AND tp.property_code =
                                           'CLEARTEXT_ACCOUNT_NUMBER'
                                    and it.TRANS_STATCODE='COMPLETE'
                                    --and it.trans_seq = 826 and it.wkstn_id = 1
                                    -- Code for Voided of PLCC paymnet line item 
                                    AND EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm tl
                                              WHERE     tl.organization_id =
                                                           ts.organization_id
                                                    AND tl.trans_seq =
                                                           ts.trans_seq
                                                    AND tl.rtl_loc_id =
                                                           ts.rtl_loc_id
                                                    AND tl.wkstn_id =
                                                           ts.wkstn_id
                                                    AND tl.business_Date =
                                                           ts.business_Date
                                                    AND tl.rtrans_lineitm_seq =
                                                           ts.rtrans_lineitm_seq
                                                    AND tl.VOID_FLAG = '0'
                                                     AND ts.item_id IN ('9999977','TCCPMNT'))
                                   --End OF PLCC payment Void Line  
                           GROUP BY it.trans_seq,
                                    tp.string_value,
                                    --       ts.sale_lineitm_typcode,
                                    ttr.tndr_statcode,
                                    ttr.amt,
                                    it.taxtotal,
                                    it.business_date,
                                    it.trans_seq,
                                    --                  im.auth_nbr,
                                    it.rtl_loc_id,
                                    it.wkstn_id,
                                    it.operator_party_id,
                                    ts.original_wkstn_id,
                                    ts.original_rtl_loc_id,
                                    ts.original_trans_seq,
                                    ts.original_trans_seq,
                                    ttr.tndr_id
                                    -- Same amount in split tender 
                                    ,tl.rtrans_lineitm_seq
                           ORDER BY tran_date, invoice_no)
                    SELECT t.record_type,
                           t.trans_seq,
                           t.account_number,
                           t.tran_cd,
                           LPAD (ABS (SUM (t.tran_amt)) * 100, '9', '0') tran_amt,
                           t.tax,
                           t.postage,
                           t.tran_date,
                           t.tran_time,
                           t.promo_cd,
                           t.auth_no,
                           t.auth_buyer_seq_no,
                           t.invoice_no,
                           t.filler_1,
                           t.ring_cd,
                           t.defer_date,
                           t.po_job_no,
                           t.region,
                           t.district,
                           t.filler_2,
                           t.store_no,
                           t.filler_3,
                           t.clerk_id,
                           t.gl_origin_cd,
                           t.gl_type_cd,
                           t.orig_invoice,
                           t.item_count,
                           t.order_date,
                           t.order_time,
                           t.payment_type,
                           t.filler_4
                      FROM temp_t1 t
                  GROUP BY t.record_type,
                           t.trans_seq,
                           t.account_number,
                           t.tran_cd,
                           t.tax,
                           t.postage,
                           t.tran_date,
                           t.tran_time,
                           t.promo_cd,
                           t.auth_no,
                           t.auth_buyer_seq_no,
                           t.invoice_no,
                           t.filler_1,
                           t.ring_cd,
                           t.defer_date,
                           t.po_job_no,
                           t.region,
                           t.district,
                           t.filler_2,
                           t.store_no,
                           t.filler_3,
                           t.clerk_id,
                           t.gl_origin_cd,
                           t.gl_type_cd,
                           t.orig_invoice,
                           t.item_count,
                           t.order_date,
                           t.order_time,
                           t.payment_type,
                           t.filler_4)
        GROUP BY record_type,
                 trans_seq,
                 -- TRANS_CNT,
                 account_number,
                 tran_cd,
                 tran_amt,
                 postage,
                 tran_date,
                 tran_time,
                 promo_cd,
                 auth_no,
                 auth_buyer_seq_no,
                 invoice_no,
                 filler_1,
                 ring_cd,
                 defer_date,
                 po_job_no,
                 region,
                 district,
                 filler_2,
                 store_no,
                 filler_3,
                 clerk_id,
                 gl_origin_cd,
                 gl_type_cd,
                 orig_invoice,
                 item_count,
                 order_date,
                 order_time,
                 payment_type,
                 filler_4) A,
                 (SELECT '30' line_ind,
                 trans_seq,
                 account_num,
                 --LISTAGG (item || quantity)
                    --WITHIN GROUP (ORDER BY account_num, item, quantity)
rtrim(xmlagg(XMLELEMENT(e,item || quantity).EXTRACT('//text()')).GetClobVal()) 
FROM (  SELECT 
                                             -- Start Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                                             CONCAT (
                           CONCAT (
                            CONCAT (
                            CONCAT(-- Same auth number issue
                              CONCAT (LPAD (tender.rtl_loc_id, '5', '0'),
                                      LPAD (tender.wkstn_id, '3', '0')),
                              LPAD (tender.trans_seq, '12', '0')),
                               LPAD (tender.string_value, '18', '0')),
                               LPAD (tender.auth_nbr, '7', '0')),
                               LPAD(tender.rtrans_lineitm_seq,'3','0'))-- Same auth number issue
                              AS trans_seq,
                                                                -- End Multiple or same PLCC card used more than once as tender type in same transaction sale/Return
                           CASE
                              WHEN tender.string_value IS NULL
                              THEN
                                 '000000000000000001'
                              ELSE
                                 LPAD (tender.string_value, '18', '0')
                           END
                              AS account_num,
                              (RPAD (lineitem.item_id, '30', ' '))
                           || (CASE
                                  WHEN lineitem.merch_level_1 = 'NP'
                                  THEN
                                     LPAD (99, '5', '0')
                                  ELSE
                                     LPAD (NVL (lineitem.merch_level_1,99), '5', '0')-- Default dept to 91
                               END)
                           || CASE
                                 WHEN tender.tndr_statcode = 'Refund'
                                 THEN
                                       '-'
                                    || LPAD ( --                              SUM(abs(lineitem.extended_amt * 100) ),
                                             ABS (lineitem.extended_amt * 100),
                                             '9',
                                             '0')
                                 WHEN tender.tndr_statcode = 'Tender'
                                 THEN
                                       '+'
                                    || LPAD ( --                               SUM(abs(lineitem.extended_amt * 100) ),
                                             ABS (lineitem.extended_amt * 100),
                                             '9',
                                             '0')
                              END
                              item,
                              REPLACE (LPAD ( --                         SUM(lineitem.quantity),
                                             lineitem.quantity, '5', '0'),
                                       '.',
                                       0)
                           || 'N'
                              quantity
                      FROM dtv.trl_sale_lineitm lineitem,
                           (SELECT DISTINCT trl.organization_id organization_id,
                                            trl.business_date business_date,
                                            trl.rtl_loc_id rtl_loc_id,
                                            trl.trans_seq trans_seq,
                                            trl.wkstn_id wkstn_id,
                                            ttr.string_value string_value,
                                            tr.tndr_statcode tndr_statcode,
                                            im.auth_nbr,
                                            im.rtrans_lineitm_seq-- Same auth number issue
                              FROM dtv.trl_rtrans_lineitm trl,
                                   dtv.trl_rtrans_lineitm_p ttr,
                                   dtv.ttr_tndr_lineitm tr,
                                   dtv.trn_trans trn,
                                   dtv.ttr_credit_debit_tndr_lineitm im
                             WHERE     trl.organization_id = ttr.organization_id
                                   AND trl.business_date = ttr.business_date
                                   AND trl.rtl_loc_id = ttr.rtl_loc_id
                                   AND trl.trans_seq = ttr.trans_seq
                                   AND trl.wkstn_id = ttr.wkstn_id
                                   AND trl.rtrans_lineitm_seq =
                                          ttr.rtrans_lineitm_seq
                                   AND property_code = 'CLEARTEXT_ACCOUNT_NUMBER'
                                   AND trl.organization_id = tr.organization_id
                                   AND trl.business_date = tr.business_date
                                   AND trl.rtl_loc_id = tr.rtl_loc_id
                                   AND trl.trans_seq = tr.trans_seq
                                   AND trl.wkstn_id = tr.wkstn_id
                                   AND trl.rtrans_lineitm_seq =
                                          tr.rtrans_lineitm_seq
                                    AND im.organization_id = tr.organization_id
                                   AND im.business_date = tr.business_date
                                   AND im.rtl_loc_id = tr.rtl_loc_id
                                   AND im.trans_seq = tr.trans_seq
                                   AND im.wkstn_id = tr.wkstn_id
                                   AND im.rtrans_lineitm_seq =
                                          tr.rtrans_lineitm_seq
                                   AND tr.tndr_id = 'PRIVATE_LABEL'
                                   AND trl.organization_id = trn.organization_id
                                   AND trl.business_date = trn.business_date
                                   AND trl.rtl_loc_id = trn.rtl_loc_id
                                   AND trl.trans_seq = trn.trans_seq
                                   AND trl.wkstn_id = trn.wkstn_id
                                   AND trn.post_void_flag = 0
                                   AND TRUNC (new_time(trl.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1)
                                   -- 5th change TRUNC (trl.business_date) = TRUNC (SYSDATE-1)
                                   AND trl.rtl_loc_id NOT IN ('5093')
                                   AND trl.rtrans_lineitm_typcode = 'TENDER'
                                    and trn.TRANS_STATCODE='COMPLETE'
                                   AND NOT EXISTS
                                          (SELECT 1
                                             FROM dtv.trl_rtrans_lineitm_p tlp
                                            WHERE     tlp.organization_id =
                                                         trl.organization_id
                                                  AND tlp.trans_seq =
                                                         trl.trans_seq
                                                  AND tlp.rtl_loc_id =
                                                         trl.rtl_loc_id
                                                  AND tlp.wkstn_id = trl.wkstn_id
                                                  AND tlp.rtrans_lineitm_seq =
                                                         trl.rtrans_lineitm_seq
                                                  AND tlp.property_code =
                                                         'WEB_ORDER_PAYMENT')
                                   AND trl.void_flag = '0' --AND trl.trans_seq=826 and trl.wkstn_id = 1
                                                          ) tender,
                           dtv.trl_rtrans_lineitm rtrn
                     WHERE     tender.organization_id = lineitem.organization_id
                           AND tender.business_date = lineitem.business_date
                           AND tender.rtl_loc_id = lineitem.rtl_loc_id
                           AND tender.trans_seq = lineitem.trans_seq
                           AND tender.wkstn_id = lineitem.wkstn_id
                           AND lineitem.sale_lineitm_typcode IN ('SALE', 'RETURN')
                           --           AND lineitem.trans_seq=224
                           AND rtrn.VOID_FLAG = 0
                           AND tender.organization_id = lineitem.organization_id
                           AND rtrn.business_date = lineitem.business_date
                           AND rtrn.rtl_loc_id = lineitem.rtl_loc_id
                           AND rtrn.trans_seq = lineitem.trans_seq
                           AND rtrn.wkstn_id = lineitem.wkstn_id
                           AND rtrn.RTRANS_LINEITM_SEQ =
                                  lineitem.RTRANS_LINEITM_SEQ
                    GROUP BY lineitem.item_id,
                           lineitem.merch_level_1,
                           lineitem.quantity,
                           lineitem.extended_amt,
                           lineitem.rtrans_lineitm_seq,
                           tender.string_value,
                           tender.tndr_statcode,
                           tender.wkstn_id,
                           tender.rtl_loc_id,
                           tender.trans_seq,
                           tender.auth_nbr,
                           tender.rtrans_lineitm_seq)-- Same auth number issue
        GROUP BY trans_seq, account_num) B
        where a.account_number=b.account_num(+) and a.tran_cd!='852') where BA is null
        UNION all
SELECT '99' line_ind,
               record_type,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               filler_1,
               sales_amt,
               sales_cnt,
               returns_amt,
               returns_cnt,
               downpmt_amt,
               downpmt_cnt,
               payment_amt,
               payment_cnt,
               filler_4,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL
          FROM (  SELECT record_type,
                         business_date,
                         filler_1,
                         LPAD (SUM (sales_amt), 13, 0) sales_amt,
                         LPAD (SUM (sales_cnt), 9, 0) sales_cnt,
                         LPAD (SUM (returns_amt), 13, 0) returns_amt,
                         LPAD (SUM (return_cnt), 9, 0) returns_cnt,
                         LPAD (SUM (downpmt_amt), 13, 0) downpmt_amt,
                         LPAD (SUM (downpmt_cnt), 9, 0) downpmt_cnt,
                         LPAD (SUM (payment_amt), 13, 0) payment_amt,
                         LPAD (SUM (payment_cnt), 9, 0) payment_cnt,
                         '                                  ' AS filler_4
                    FROM ( (  SELECT '99' AS record_type,
                                     TO_CHAR ( TRUNC(new_time(trn.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy')
                                        business_date,
                                     '999999999999999999' AS filler_1,
                                     LPAD (
                                        TO_CHAR (
                                           SUM (
                                              CASE
                                                 WHEN tr.tndr_statcode = 'Tender'
                                                 THEN
                                                    tr.amt * 100
                                                 ELSE
                                                    0
                                              END)),
                                        '13',
                                        '0')
                                        sales_amt,
                                     LPAD (
                                        TO_CHAR (
                                           COUNT (
                                              CASE
                                                 WHEN tr.tndr_statcode = 'Tender'
                                                 THEN
                                                    tr.trans_seq
                                              END)),
                                        9,
                                        0)
                                        sales_cnt,
                                     LPAD (
                                        TO_CHAR (
                                           SUM (
                                              CASE
                                                 WHEN tr.tndr_statcode = 'Refund'
                                                 THEN
                                                    ABS (tr.amt * 100)
                                                 ELSE
                                                    0
                                              END)),
                                        '13',
                                        '0')
                                        returns_amt,
                                     LPAD (
                                        TO_CHAR (
                                           COUNT (
                                              CASE
                                                 WHEN tr.tndr_statcode = 'Refund'
                                                 THEN
                                                    tr.trans_seq
                                              END)),
                                        9,
                                        0)
                                        return_cnt,
                                     LPAD (TO_CHAR ('0'), '13', '0') downpmt_amt,
                                     LPAD (TO_CHAR ('0'), 9, 0) downpmt_cnt,
                                     LPAD (TO_CHAR ('0'), '13', '0') payment_amt,
                                     LPAD (TO_CHAR ('0'), 9, 0) payment_cnt,
                                     '                                  '
                                        AS filler_4
                                FROM dtv.ttr_tndr_lineitm tr,
                                     dtv.trn_trans trn,
                                     dtv.trl_rtrans_lineitm trl
                               WHERE     tr.organization_id = trn.organization_id
                                     AND tr.organization_id = trl.organization_id
                                     AND tr.rtl_loc_id = trn.rtl_loc_id
                                     AND tr.rtl_loc_id = trl.rtl_loc_id
                                     AND tr.business_date = trn.business_date
                                     AND tr.business_date = trl.business_date
                                     AND tr.trans_seq = trn.trans_seq
                                     AND tr.trans_seq = trl.trans_seq
                                     AND tr.wkstn_id = trn.wkstn_id
                                     AND tr.wkstn_id = trl.wkstn_id
                                     AND tr.rtrans_lineitm_seq =
                                            trl.rtrans_lineitm_seq
                                     AND TRUNC(new_time(trn.CREATE_DATE,'GMT','PST')) =
                                            TRUNC (SYSDATE-1)
                                     AND tr.rtl_loc_id NOT IN ('5093')
                                     AND tr.organization_id = '1'
                                     AND trn.post_void_flag = 0
                                     AND trn.trans_statcode = 'COMPLETE'
                                     AND trn.trans_typcode = 'RETAIL_SALE'
                                     AND trl.void_flag = 0
                                     AND tr.tndr_id = 'PRIVATE_LABEL'
                                     AND trl.rtrans_lineitm_typcode IN ('TENDER')
                                     -- and tr.trans_seq = 826 and tr.wkstn_id = 1
                                     AND EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm_p tlp
                                              WHERE     tlp.organization_id =
                                                           trl.organization_id
                                                    AND tlp.trans_seq =
                                                           trl.trans_seq
                                                    AND tlp.rtl_loc_id =
                                                           trl.rtl_loc_id
                                                    AND tlp.wkstn_id =
                                                           trl.wkstn_id
                                                    AND tlp.business_Date =
                                                           trl.business_Date
                                                    AND tlp.rtrans_lineitm_seq =
                                                           trl.rtrans_lineitm_seq
                                                    AND tlp.PROPERTY_CODE =
                                                           'CLEARTEXT_ACCOUNT_NUMBER')
                                     AND EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm tl
                                              WHERE     tl.organization_id =
                                                           trl.organization_id
                                                    AND tl.trans_seq =
                                                           trl.trans_seq
                                                    AND tl.rtl_loc_id =
                                                           trl.rtl_loc_id
                                                    AND tl.wkstn_id =
                                                           trl.wkstn_id
                                                    AND tl.business_Date =
                                                           trl.business_Date
                                                    AND tl.rtrans_lineitm_seq =
                                                           trl.rtrans_lineitm_seq
                                                    AND tl.VOID_FLAG = '0')
                                     AND NOT EXISTS
                                            (SELECT 1
                                               FROM dtv.trl_rtrans_lineitm_p tlp
                                              WHERE     tlp.organization_id =
                                                           trl.organization_id
                                                    AND tlp.trans_seq =
                                                           trl.trans_seq
                                                    AND tlp.rtl_loc_id =
                                                           trl.rtl_loc_id
                                                    AND tlp.wkstn_id =
                                                           trl.wkstn_id
                                                    AND tlp.business_Date =
                                                           trl.business_Date
                                                    AND tlp.rtrans_lineitm_seq =
                                                           trl.rtrans_lineitm_seq
                                                    AND tlp.property_code =
                                                           'WEB_ORDER_PAYMENT')
                            GROUP BY TO_CHAR ( TRUNC(new_time(trn.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy'))
                          UNION all
                          (  SELECT '99' AS record_type,
                                    TO_CHAR ( TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy')
                                       business_date,
                                    '999999999999999999' AS filler_1,
                                    LPAD (TO_CHAR ('0'), '13', '0') sales_amt,
                                    LPAD (TO_CHAR ('0'), 9, 0) sales_cnt,
                                    LPAD (TO_CHAR ('0'), '13', '0') returns_amt,
                                    LPAD (TO_CHAR ('0'), 9, 0) return_cnt,
                                    LPAD (TO_CHAR ('0'), '13', '0') downpmt_amt,
                                    LPAD (TO_CHAR ('0'), 9, 0) downpmt_cnt,
                                    LPAD (
                                       TO_CHAR (
                                          SUM (
                                             CASE
                                                WHEN item_id  in('9999977','TCCPMNT')
                                                THEN
                                                   extended_amt * 100
                                                ELSE
                                                   0
                                             END)),
                                       '13',
                                       '0')
                                       payment_amt,
                                    LPAD (
                                       TO_CHAR ((SELECT count( rtl_loc_id|| LPAD (TO_CHAR (wkstn_id), 3, 0)|| LPAD (TO_CHAR (TRANS_SEQ), 12, 0)) from(
                                               select tl.rtl_loc_id,tl.wkstn_id,Tl.TRANS_SEQ,  tndr_id,sum(amt)
                                                FROM DTV.TRL_RTRANS_LINEITM TL ,DTV.TTR_TNDR_LINEITM trl
                                                  where TRUNC(new_time(tl.CREATE_DATE,'GMT','PST')) = TRUNC (SYSDATE-1) 
                                                  and tl.VOID_FLAG = '0'
                                                  AND TL.RTRANS_LINEITM_TYPCODE='TENDER'
                                                   AND TRL.TNDR_STATCODE='Tender' 
                                                   AND tl.organization_id =trl.organization_id
                                                   AND tl.trans_seq =trl.trans_seq
                                                   AND tl.rtl_loc_id = trl.rtl_loc_id
                                                   AND tl.wkstn_id = trl.wkstn_id
                                                   AND tl.business_Date =trl.business_Date
                                                   AND tl.rtrans_lineitm_seq = trl.rtrans_lineitm_seq
                                                   -- Start of PLCC payment line void 
                                                                                                                            and tl.void_flag='0'
                                                   and EXISTS
                                                   (
                                                   select 1 from DTV.trl_sale_lineitm TSL,DTV.TRL_RTRANS_LINEITM TM where 
                                                   tl.organization_id =TSL.organization_id
                                                   AND tl.trans_seq =TSL.trans_seq
                                                   AND tl.rtl_loc_id = TSL.rtl_loc_id
                                                   AND tl.wkstn_id = TSL.wkstn_id
                                                   AND tl.business_Date =TSL.business_Date
                                                   AND TM.organization_id =TSL.organization_id
                                                   AND TM.trans_seq =TSL.trans_seq
                                                   AND TM.rtl_loc_id = TSL.rtl_loc_id
                                                   AND TM.wkstn_id = TSL.wkstn_id
                                                   AND TM.business_Date =TSL.business_Date
                                                   AND TM.rtrans_lineitm_seq = TSL.rtrans_lineitm_seq                                                    
                                                   AND tm.void_flag='0'
                                                                                                                           -- END of PLCC payment line void
                                                    AND TSL.item_id  in('9999977','TCCPMNT')
                                                   ) group by tl.rtl_loc_id,tl.wkstn_id,Tl.TRANS_SEQ,  tndr_id)
                                                   )),
                                       9,
                                       0)
                                       payment_cnt,
                                    '                                  '
                                       AS filler_4
                               FROM dtv.trl_sale_lineitm trl, dtv.trn_trans th
                              WHERE     TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')) =
                                           TRUNC (SYSDATE-1)
                                    AND trl.rtl_loc_id NOT IN ('5093')
                                    AND trl.item_id in('9999977','TCCPMNT')
                                    AND trl.trans_seq = th.trans_seq
                                    AND trl.rtl_loc_id = th.rtl_loc_id
                                    AND trl.wkstn_id = th.wkstn_id
                                    AND trl.business_date = th.business_date
                                    AND th.TRANS_STATCODE = 'COMPLETE'
                                    AND EXISTS
                                           (SELECT 1
                                              FROM dtv.trl_rtrans_lineitm_p tlp
                                             WHERE     tlp.organization_id =
                                                          trl.organization_id
                                                   AND tlp.trans_seq =
                                                          trl.trans_seq
                                                   AND tlp.rtl_loc_id =
                                                          trl.rtl_loc_id
                                                   AND tlp.wkstn_id =
                                                          trl.wkstn_id
                                                   AND tlp.business_Date =
                                                          trl.business_Date
                                                   AND tlp.rtrans_lineitm_seq =
                                                          trl.rtrans_lineitm_seq
                                                   AND tlp.PROPERTY_CODE =
                                                          'CLEARTEXT_ACCOUNT_NUMBER')
                                    AND EXISTS
                                           (SELECT 1
                                              FROM dtv.trl_rtrans_lineitm tl
                                             WHERE     tl.organization_id =
                                                          trl.organization_id
                                                   AND tl.trans_seq =
                                                          trl.trans_seq
                                                   AND tl.rtl_loc_id =
                                                          trl.rtl_loc_id
                                                   AND tl.wkstn_id = trl.wkstn_id
                                                   AND tl.business_Date =
                                                          trl.business_Date
                                                   AND tl.rtrans_lineitm_seq =
                                                          trl.rtrans_lineitm_seq
                                                    AND trl.item_id in('9999977','TCCPMNT')
                                                   AND tl.VOID_FLAG = '0')
                           GROUP BY TO_CHAR (TRUNC(new_time(trl.CREATE_DATE,'GMT','PST')), 'dd-mon-yyyy')))
                GROUP BY record_type, business_date, filler_1))
        ORDER BY trans_seq,RNK, line_ind);
spool off;
   exit; 
END

sqlStat=$?

if [ ${sqlStat} -ne 0 ]; then

echo "${exedate} Program: ${pgmName}: Aborted" >> ${LOGFILE}

    if [ `grep -c "ORA-" ${sqllog}` -gt 0 ]; then
  
      cat ${sqllog} >> ${sqlerror}

     echo "${exedate} Program: ${pgmName}: Aborted" >> ${LOGFILE} 
      
exit -1
fi
exit -1

else
      echo "${exedate} Program: ${pgmName}: :: Terminated Successfully" >> ${LOGFILE} 
      #pgpencrypter.bat
     cd /opt/scripts/plccSettlement/pgpencrypter/build/bin  
     sh pgpencrypter.sh
     javaStat=$?

if [ ${javaStat} -ne 0 ]; then
    echo "execution got failed ." >> ${LOGFILE}
      else
     echo "execution  completed " >> ${LOGFILE}
#     cd /opt/scripts/plccSettlement/pgpencrypter/PLCCSettlementSFTPUpload
#sh sftpUpload.sh
      fi

fi


echo "---------------------------------------------------" >> $LOGFILE
echo "Processing end at:"`date`                        >> $LOGFILE
echo "---------------------------------------------------" >> $LOGFILE
echo 'End of Batch call script'>>$LOGFILE
exit 0
